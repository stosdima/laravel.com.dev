<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NavigationMenu extends Model
{
    protected $table = 'navigation_menu';

    protected $fillable = ['child_name', 'child_href'];

    function childs()
    {
        return $this->hasMany('App\NavigationMenu','parent_id', 'id');
    }
}
