<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'model', 'price', 'size', 'type', 'brand', 'description', 'quantity'];

    public function subcategories()
    {
        return $this->belongsToMany('App\SubCategory');
    }

}
