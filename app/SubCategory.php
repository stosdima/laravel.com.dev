<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_category';

    protected $primaryKey = 'id';

    public function products()
    {
        return $this->belongsToMany('App\Products');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories');
    }
}
