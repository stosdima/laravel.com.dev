<?php

namespace App\Http\Controllers;

use App\Categories;
use App\NavigationMenu;
use App\Products;
use Illuminate\Http\Request;
use App\SubCategory;

class IndexController extends Controller
{
    public function index()
    {
        $categories = Categories::all();

        return view('index')->with(['categories' => $categories]);
    }
}
