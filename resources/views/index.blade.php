@extends('layouts.main')
@section('content')
<!--/banner-->
<div id="cate" class="categories">
	 <div class="container">
		 <h3>CATEGORIES</h3>
		 <div class="categorie-grids">
			 @foreach($categories as $category)
			 	<a href="bicycles.html"><div class="col-md-4 cate-grid grid1">
				 <h4>{{$category['name']}}</h4>
				 <p>{{$category['description']}}</p>
				 <a class="store" href="bicycles.html">GO TO STORE</a>
			 </div></a>
			 @endforeach
			 <div class="clearfix"></div>
		 </div>
	 </div>
</div>
<!--bikes-->
<div class="bikes">	
		 <h3>POPULAR BIKES</h3>
		 <div class="bikes-grids">			 
			 <ul id="flexiselDemo1">
				 @foreach($categories as $category)
					 @foreach($category->subcategories as $subCat)
						 @foreach($subCat->products as $product)
				 	<li>
					 <img src="{!! $product['img_url'] !!}" alt=""/>
					 <div class="bike-info">
						 <div class="model">
							 <h4>FIXED GEAR<span>${{$product['price']}}</span></h4>
						 </div>
						 <div class="model-info">
						     <select>
							  <option value="volvo">{{$product['size']}}</option>
							 </select>
							 <a href="bicycles.html">BUY</a>
						 </div>						 
						 <div class="clearfix"></div>
					 </div>
					 <div class="viw">
						<a href="bicycles.html">Quick View</a>
					 </div>
				 </li>
						@endforeach
					@endforeach
				@endforeach
		    </ul>
			<script type="text/javascript">
			 $(window).load(function() {			
			  $("#flexiselDemo1").flexisel({
				visibleItems: 3,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover:true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems: 2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 3
					}
				}
			});
			});
			</script>
			<script type="text/javascript" src="{!! asset('public/js/jquery.flexisel.js') !!}"></script>
	</div>
</div>
<!---->
<div class="contact">
	<div class="container">
		<h3>CONTACT US</h3>
		<p>Please contact us for all inquiries and purchase options.</p>
		<form>
			 <input type="text" placeholder="NAME" required="">
			 <input type="text" placeholder="SURNAME" required="">			 
			 <input class="user" type="text" placeholder="USER@DOMAIN.COM" required=""><br>
			 <textarea placeholder="MESSAGE"></textarea>
			 <input type="submit" value="SEND">
		</form>
	</div>
</div>
<!---->
@stop



